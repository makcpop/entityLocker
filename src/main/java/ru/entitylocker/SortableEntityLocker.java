package ru.entitylocker;

import java.util.Comparator;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * {@link EntityLocker} implementation. Work with any type of id.
 * The implementation allow reentrant locking. The class is based on {@link ReentrantEntityLocker}, but adds
 * special lock for list lock. The list will be sorted before locking.
 *
 * Id must be comparable.
 *
 * The constructor of the class is accept optional <em>fairness</em> parameter.
 *
 * @param <T> id type
 */
public class SortableEntityLocker<T extends Comparable<? super T>> implements EntityLocker<T> {

    private final ReentrantEntityLocker<T> entityLocker;

    public SortableEntityLocker() {
        this(false);
    }

    public SortableEntityLocker(boolean fair) {
        this.entityLocker = new ReentrantEntityLocker(fair);
    }

    /**
     * Acquires the lock on the id.
     *
     * @param id id for lock
     *
     * @throws IllegalArgumentException if id is null
     */
    @Override
    public void lock(T id) {
        entityLocker.lock(id);
    }

    /**
     * Try to acquires lock one the id.
     *
     * @param id id for lock
     *
     * @return true if the id is locked, false - is not locked.
     * @throws InterruptedException if the current thread is interrupted
     *         while acquiring the lock (and interruption of lock
     *         acquisition is supported)
     * @throws IllegalArgumentException if id is null
     */
    @Override
    public boolean tryLock(T id) throws InterruptedException {
        return entityLocker.tryLock(id);
    }

    /**
     * Try to acquires lock one the id within the specified time.
     *
     * @param id id for lock
     * @param time waiting time. Time mustn't be less than 0
     * @param unit waiting time unit
     *
     * @return true if the id is locked, false - is not locked.
     * @throws InterruptedException if the current thread is interrupted
     *         while acquiring the lock (and interruption of lock
     *         acquisition is supported)
     * @throws IllegalArgumentException if id is null
     */
    @Override
    public boolean tryLock(T id, long time, TimeUnit unit) throws InterruptedException {
        return entityLocker.tryLock(id, time, unit);
    }

    /**
     * Acquires locks on the id's list. Ignore null elements.
     *
     * List wil be sorted via natural order before locking.
     * This method is blocking. Use {@link ReentrantEntityLocker#tryLock(List)} or
     * {@link ReentrantEntityLocker#tryLock(List)}
     *
     * @param ids id's list for lock
     *
     * @throws IllegalArgumentException if id's list is null
     */
    @Override
    public void lock(List<T> ids) {
        entityLocker.lock(ids, Comparator.naturalOrder());
    }

    /**
     * Try to acquires lock one all ids from list. List will be sorted
     * by comparator before the locking.
     * Null elements in list are ignored.
     *
     * List will be sorted with natural order before locking.
     *
     *
     * @param ids id's list for lock
     *
     * @return true if all ids are locked, false - at least one id is not locked.
     * @throws InterruptedException if the current thread is interrupted
     *         while acquiring the lock (and interruption of lock
     *         acquisition is supported)
     * @throws IllegalArgumentException if id's list is null
     */
    @Override
    public boolean tryLock(List<T> ids) throws InterruptedException {
        return entityLocker.tryLock(ids, Comparator.naturalOrder());
    }

    /**
     * Try to acquires lock one all ids from list within the specified time for each elements. List will be sorted
     * by comparator before the locking.
     * Null elements in list are ignored.
     *
     * The list will be sorted before locking.
     *
     * @param ids id's list for lock
     * @param time waiting time. Time mustn't be less than 0
     * @param unit waiting time unit
     *
     * @return true if all ids are locked, false - at least one id is not locked.
     *
     * @throws IllegalArgumentException if id's list is null
     */
    @Override
    public boolean tryLock(List<T> ids, long time, TimeUnit unit) throws InterruptedException {
        return entityLocker.tryLock(ids, Comparator.naturalOrder(), time, unit);
    }

    /**
     * Acquires the locks on the id's list, which will be sorted by comparator. Null elements are ignored.
     *
     * If comparator is null, the list won't be sorted.
     * This method is blocking. Use {@link ReentrantEntityLocker#tryLock(List, Comparator)} or
     * {@link ReentrantEntityLocker#tryLock(List, Comparator)}
     *
     * @param ids id's list for lock
     * @param comparator comparator for sorting
     *
     * @throws IllegalArgumentException if id is null
     */
    @Override
    public void lock(List<T> ids, Comparator<? super T> comparator) {
        entityLocker.lock(ids, comparator);
    }

    /**
     * Try to acquires lock one all ids from list. List will be sorted
     * by comparator before the locking.
     * Null elements in list are ignored.
     * If comparator is null, the list won't be sorted.
     *
     *
     * @param ids id's list for lock
     * @param comparator comparator for sorting
     *
     * @return true if all ids are locked, false - at least one id is not locked.
     * @throws InterruptedException if the current thread is interrupted
     *         while acquiring the lock (and interruption of lock
     *         acquisition is supported)
     * @throws IllegalArgumentException if id's list is null
     */
    @Override
    public boolean tryLock(List<T> ids, Comparator<? super T> comparator) throws InterruptedException {
        return entityLocker.tryLock(ids, comparator);
    }

    /**
     * Try to acquires lock one all ids from list within the specified time for each elements. List will be sorted
     * by comparator before the locking.
     * Null elements in list are ignored.
     * If comparator is null, the list won't be sorted.
     *
     *
     * @param ids id's list for lock
     * @param comparator comparator for sorting
     * @param time waiting time. Time mustn't be less than 0
     * @param unit waiting time unit
     *
     * @return true if all ids are locked, false - at least one id is not locked.
     *
     * @throws IllegalArgumentException if id's list is null
     */
    @Override
    public boolean tryLock(List<T> ids, Comparator<? super T> comparator, long time, TimeUnit unit) throws InterruptedException {
        return entityLocker.tryLock(ids, comparator, time, unit);
    }

    /**
     * Release the lock on the id.
     *
     * @param id id for lock
     *
     * @throws IllegalArgumentException if id is null
     * @throws IllegalMonitorStateException if the current thread does not
     *         hold this lock or id of id never was locked
     */
    @Override
    public void unlock(T id) {
        entityLocker.unlock(id);
    }

    /**
     * Releases locks on the id's list. Ignore null elements.
     *
     * @param ids id's list for lock
     *
     * @throws IllegalArgumentException if id's list is null
     * @throws IllegalMonitorStateException if the current thread does not
     *         hold this lock or any of ids never was locked
     */
    @Override
    public void unlock(List<T> ids) {
        entityLocker.unlock(ids, Comparator.naturalOrder());
    }

    /**
     * Release locks on all ids from list, which will be sorted by comparator. Null elements are ignored.
     *
     * If comparator is null, the list won't be sorted.
     *
     * @param ids id's list for unlocking
     * @param comparator comparator for sorting
     *
     * @throws IllegalArgumentException if id is null
     * @throws IllegalMonitorStateException if the current thread does not
     *         hold this lock or any of ids never was locked
     */
    @Override
    public void unlock(List<T> ids, Comparator<? super T> comparator) {
        entityLocker.unlock(ids, comparator);
    }

    /**
     * Execute the supplier and return the value. Lock on the id is acquired while the supplier is executing.
     * The method uses blocking lock.
     *
     * @param id id for locking
     * @param supplier
     * @param <R> supplier's return type
     * @return the result of supplier
     *
     * @throws IllegalArgumentException if id or supplier are null
     */
    @Override
    public <R> R execute(T id, Supplier<R> supplier) {
        return entityLocker.execute(id, supplier);
    }

    /**
     * Execute the consumer. Lock on the id is acquired while the consumer is executing.
     * The method uses blocking lock.
     *
     * @param id id for locking
     * @param consumer
     *
     * @throws IllegalArgumentException if id or consumer are null
     */
    @Override
    public void execute(T id, Consumer<? super T> consumer) {
        entityLocker.execute(id, consumer);
    }

    /**
     * Execute the function and return the value. Lock on the id is acquired while the function is executing.
     * The method uses blocking lock.
     *
     * @param id id for locking
     * @param function
     * @param <R> function's return type
     * @return the result of function
     *
     * @throws IllegalArgumentException if id or function are null
     */
    @Override
    public <R> R execute(T id, Function<? super T, R> function) {
        return entityLocker.execute(id, function);
    }

    /**
     * Execute the supplier and return the value. Locks on all id's from list are
     * acquired while the supplier is executing. Null elements are ignored.
     *
     * List is used as is. Use sorted list or {@link ReentrantEntityLocker#execute(List, Comparator, Supplier)}
     * to prevent deadlock.
     *
     * The method uses list's copy for locking.
     * The method uses blocking lock.
     *
     * @param ids id's list for locking
     * @param supplier
     * @param <R> supplier's return type
     * @return the result of supplier
     *
     * @throws IllegalArgumentException if id's list or supplier are null
     */
    @Override
    public <R> R execute(List<T> ids, Supplier<R> supplier) {
        return entityLocker.execute(ids, Comparator.naturalOrder(), supplier);
    }

    /**
     * Execute the consumer. Locks on all id's from list are
     * acquired while the consumer is executing. Null elements are ignored.
     *
     * List is used as is. Use sorted list or {@link ReentrantEntityLocker#execute(List, Comparator, Consumer)}
     * to prevent deadlock.
     *
     * The method uses list's copy for locking.
     * The method uses blocking lock.
     *
     * @param ids id's list for locking
     * @param consumer
     *
     * @throws IllegalArgumentException if id's list or consumer are null
     */
    @Override
    public void execute(List<T> ids, Consumer<? super List<? extends T>> consumer) {
        entityLocker.execute(ids, Comparator.naturalOrder(), consumer);
    }

    /**
     * Execute the function. Locks on all id's from list are
     * acquired while the supplier is executing. Null elements are ignored.
     *
     * List is used as is. Use sorted list or {@link ReentrantEntityLocker#execute(List, Comparator, Function)}
     * to prevent deadlock.
     *
     * The method uses list's copy for locking.
     * The method uses blocking lock.
     *
     * @param ids id's list for locking
     * @param function
     * @param <R> function's return type
     * @return the result of function
     *
     * @throws IllegalArgumentException if id's list or function are null
     */
    @Override
    public <R> R execute(List<T> ids, Function<? super List<? extends T>, R> function) {
        return execute(ids, Comparator.naturalOrder(), function);
    }

    /**
     * Execute the supplier and return the value. Locks on all id's from list(sorted by comparator) are acquired while
     * the supplier is executing. Null elements are ignored.
     *
     * If comparator is null, the list won't be sorted.
     *
     * The method uses list's copy for locking.
     * The method uses blocking lock.
     *
     * @param ids id's list for locking
     * @param comparator comparator for sorting
     * @param supplier
     * @param <R> supplier's return type
     * @return the result of supplier
     *
     * @throws IllegalArgumentException if id's list or supplier are null
     */
    @Override
    public <R> R execute(List<T> ids, Comparator<? super T> comparator, Supplier<R> supplier) {
        return entityLocker.execute(ids, comparator, supplier);
    }

    /**
     * Execute the consumer. Locks on all id's from list(sorted by comparator) are acquired while the consumer is executing.
     * Null elements are ignored.
     *
     * If comparator is null, the list won't be sorted.
     *
     * The method uses list's copy for locking.
     * The method uses blocking lock.
     *
     * @param ids id's list for locking
     * @param consumer
     *
     * @throws IllegalArgumentException if id's list or consumer are null
     */
    @Override
    public void execute(List<T> ids, Comparator<? super T> comparator, Consumer<? super List<? extends T>> consumer) {
        entityLocker.execute(ids, comparator, consumer);
    }

    /**
     * Execute the function and return the value. Locks on all id's from list(sorted by comparator) are
     * acquired while the function is executing.Null elements are ignored.
     *
     * If comparator is null, the list won't be sorted.
     *
     * The method uses list's copy for locking.
     * The method uses blocking lock.
     *
     * @param ids id's list for locking
     * @param comparator
     * @param function
     * @param <R> function's return type
     * @return the result of function
     *
     * @throws IllegalArgumentException if id's list or function are null
     */
    @Override
    public <R> R execute(List<T> ids, Comparator<? super T> comparator, Function<? super List<? extends T>, R> function) {
        return entityLocker.execute(ids, comparator, function);
    }

    /**
     * Method for cleaning used locks.
     */
    public void clearNotLockedLocks() {
        entityLocker.clearNotLockedLocks();
    }
}
