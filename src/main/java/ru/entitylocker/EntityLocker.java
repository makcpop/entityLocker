package ru.entitylocker;

import java.util.Comparator;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * Interface for lock entity class.
 *
 * @param <T>
 *
 * @author Maksim Popov
 */
public interface EntityLocker<T> {

    /**
     * Acquires the lock on the id.
     *
     * @param id id for lock
     */
    void lock(T id);

    /**
     * Try to acquires lock one the id within the specified time.
     *
     * @param id id for lock
     * @param time waiting time
     * @param unit waiting time unit
     *
     * @return true if the id is locked, false - is not locked.
     * @throws InterruptedException if the current thread is interrupted
     *         while acquiring the lock (and interruption of lock
     *         acquisition is supported)
     */
    boolean tryLock(T id, long time, TimeUnit unit) throws InterruptedException;

    /**
     * Try to acquires lock one the id.
     *
     * @param id id for lock
     *
     * @return true if the id is locked, false - is not locked.
     * @throws InterruptedException if the current thread is interrupted
     *         while acquiring the lock (and interruption of lock
     *         acquisition is supported)
     */
    boolean tryLock(T id) throws InterruptedException;

    /**
     * Acquires the locks on the id's list.
     *
     * @param ids id's list for lock
     */
    void lock(List<T> ids);

    /**
     * Try to acquires lock one all ids from list within the specified time for each elements.
     *
     * @param ids id's list for lock
     * @param time waiting time
     * @param unit waiting time unit
     *
     * @return true if all ids are locked, false - at least one id is not locked.
     * @throws InterruptedException if the current thread is interrupted
     *         while acquiring the lock (and interruption of lock
     *         acquisition is supported)
     */
    boolean tryLock(List<T> ids, long time, TimeUnit unit) throws InterruptedException;

    /**
     * Try to acquires lock one all ids from list.
     *
     * @param ids id's list for lock
     *
     * @return true if all ids are locked, false - at least one id is not locked.
     * @throws InterruptedException if the current thread is interrupted
     *         while acquiring the lock (and interruption of lock
     *         acquisition is supported)
     */
    boolean tryLock(List<T> ids) throws InterruptedException;

    /**
     * Acquires the locks on the id's list, which will be sorted by comparator.
     *
     * @param ids id's list for lock
     * @param comparator comparator for sorting
     */
    void lock(List<T> ids, Comparator<? super T> comparator);

    /**
     * Try to acquires lock one all ids from list within the specified time for each elements. List will be sorted
     * by comparator before the locking.
     *
     * @param ids id's list for lock
     * @param comparator comparator for sorting
     * @param time waiting time
     * @param unit waiting time unit
     *
     * @return true if all ids are locked, false - at least one id is not locked.
     * @throws InterruptedException if the current thread is interrupted
     *         while acquiring the lock (and interruption of lock
     *         acquisition is supported)
     */
    boolean tryLock(List<T> ids, Comparator<? super T> comparator, long time, TimeUnit unit) throws InterruptedException;

    /**
     * Try to acquires lock one all ids from list. List will be sorted
     * by comparator before the locking.
     *
     * @param ids id's list for lock
     * @param comparator comparator for sorting
     *
     * @return true if all ids are locked, false - at least one id is not locked.
     * @throws InterruptedException if the current thread is interrupted
     *         while acquiring the lock (and interruption of lock
     *         acquisition is supported)
     */
    boolean tryLock(List<T> ids, Comparator<? super T> comparator) throws InterruptedException;

    /**
     * Release the lock on the id.
     *
     * @param id id for unlocking
     */
    void unlock(T id);

    /**
     * Release locks on all ids from list.
     *
     * @param ids id's list for unlocking
     */
    void unlock(List<T> ids);

    /**
     * Release locks on all ids from list, which will be sorted by comparator.
     *
     * @param ids id's list for unlocking
     * @param comparator comparator for sorting
     */
    void unlock(List<T> ids, Comparator<? super T> comparator);

    /**
     * Execute the supplier and return the value. Lock on the id is acquired while the supplier is executing.
     *
     * @param id id for locking
     * @param supplier
     * @param <R> supplier's return type
     * @return the result of supplier
     */
    <R> R execute(T id, Supplier<R> supplier);

    /**
     * Execute the consumer. Lock on the id is acquired while the consumer is executing.
     *
     * @param id id for locking
     * @param consumer
     */
    void execute(T id, Consumer<? super T> consumer);

    /**
     * Execute the function and return the value. Lock on the id is acquired while the function is executing.
     *
     * @param id id for locking
     * @param function
     * @param <R> function's return type
     * @return the result of function
     */
    <R> R execute(T id, Function<? super T, R> function);

    /**
     * Execute the supplier and return the value. Locks on all id's from list are acquired while the supplier is executing.
     *
     * @param ids id's list for locking
     * @param supplier
     * @param <R> supplier's return type
     * @return the result of supplier
     */
    <R> R execute(List<T> ids, Supplier<R> supplier);

    /**
     * Execute the consumer. Locks on all id's from list are acquired while the consumer is executing.
     *
     * @param ids id's list for locking
     * @param consumer
     */
    void execute(List<T> ids, Consumer<? super List<? extends T>> consumer);

    /**
     * Execute the function and return the value. Locks on all id's from list are acquired while the function is executing.
     *
     * @param ids id's list for locking
     * @param function
     * @param <R> function's return type
     * @return the result of function
     */
    <R> R execute(List<T> ids, Function<? super List<? extends T>, R> function);

    /**
     * Execute the supplier and return the value. Locks on all id's from list(sorted by comparator) are acquired while
     * the supplier is executing.
     *
     * @param ids id's list for locking
     * @param supplier
     * @param <R> supplier's return type
     * @return the result of supplier
     */
    <R> R execute(List<T> ids, Comparator<? super T> comparator, Supplier<R> supplier);

    /**
     * Execute the consumer. Locks on all id's from list(sorted by comparator) are acquired while the consumer is executing.
     *
     * @param ids id's list for locking
     * @param consumer
     */
    void execute(List<T> ids, Comparator<? super T> comparator, Consumer<? super List<? extends T>> consumer);

    /**
     * Execute the function and return the value. Locks on all id's from list(sorted by comparator) are
     * acquired while the function is executing.
     *
     * @param ids id's list for locking
     * @param function
     * @param comparator
     * @param <R> function's return type
     * @return the result of function
     */
    <R> R execute(List<T> ids, Comparator<? super T> comparator, Function<? super List<? extends T>, R> function);
}
