package ru.entitylocker;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/**
 * {@link EntityLocker} implementation. Work with any type of id.
 * The implementation allow reentrant locking. The class uses {@link ReentrantLock} for locking id.
 *
 * Any type of id is allowed.
 *
 * The constructor of the class is accept optional <em>fairness</em> parameter.
 *
 * If you override one of {@link ReentrantEntityLocker#createLock(Object)} or {@link ReentrantEntityLocker#shouldBeRemoved(Lock)},
 * the second one have to be overriden too.
 *
 * @param <T> id type
 */
public class ReentrantEntityLocker<T> implements EntityLocker<T> {

    private final Map<T, Lock> locks = new ConcurrentHashMap<>();
    private final ReentrantReadWriteLock readWriteLocksLock = new ReentrantReadWriteLock();

    private final boolean fair;

    public ReentrantEntityLocker() {
        this(false);
    }

    public ReentrantEntityLocker(boolean fair) {
        this.fair = fair;
    }

    /**
     * Acquires the lock on the id.
     *
     * @param id id for lock
     *
     * @throws IllegalArgumentException if id is null
     */
    @Override
    public void lock(T id) {
        checkForNull(id, "Id mustn't be null");
        readWriteLocksLock.readLock().lock();
        try {
            getLock(id).lock();
        }
        finally {
            readWriteLocksLock.readLock().unlock();
        }
    }

    /**
     * Release the lock on the id.
     *
     * @param id id for lock
     *
     * @throws IllegalArgumentException if id is null
     * @throws IllegalMonitorStateException if the current thread does not
     *         hold this lock or id of id never was locked
     */
    @Override
    public void unlock(T id) {
        checkForNull(id, "Id mustn't be null");
        Lock lock = locks.get(id);
        if (lock == null) {
            throw new IllegalMonitorStateException("Id " + id + " never was locked");
        }
        lock.unlock();
    }

    /**
     * Execute the supplier and return the value. Lock on the id is acquired while the supplier is executing.
     * The method uses blocking lock.
     *
     * @param id id for locking
     * @param supplier
     * @param <R> supplier's return type
     * @return the result of supplier
     *
     * @throws IllegalArgumentException if id or supplier are null
     */
    @Override
    public <R> R execute(T id, Supplier<R> supplier) {
        checkForNull(supplier, "Supplier mustn't be null");
        lock(id);
        try {
            return supplier.get();
        }
        finally {
            unlock(id);
        }
    }

    /**
     * Execute the consumer. Lock on the id is acquired while the consumer is executing.
     * The method uses blocking lock.
     *
     * @param id id for locking
     * @param consumer
     *
     * @throws IllegalArgumentException if id or consumer are null
     */
    @Override
    public void execute(T id, Consumer<? super T> consumer) {
        checkForNull(consumer, "Consumer mustn't be null");
        lock(id);
        try {
            consumer.accept(id);
        }
        finally {
            unlock(id);
        }
    }

    /**
     * Execute the function and return the value. Lock on the id is acquired while the function is executing.
     * The method uses blocking lock.
     *
     * @param id id for locking
     * @param function
     * @param <R> function's return type
     * @return the result of function
     *
     * @throws IllegalArgumentException if id or function are null
     */
    @Override
    public <R> R execute(T id, Function<? super T, R> function) {
        checkForNull(function, "Function mustn't be null");
        lock(id);
        try {
            return function.apply(id);
        }
        finally {
            unlock(id);
        }
    }

    /**
     * Acquires locks on the id's list. Ignore null elements.
     *
     * List is used as is. Use sorted list or {@link ReentrantEntityLocker#lock(List, Comparator)}
     * to prevent deadlock.
     *
     * This method is blocking. use {@link ReentrantEntityLocker#tryLock(List)} or
     * {@link ReentrantEntityLocker#tryLock(List)}
     *
     * @param ids id's list for lock
     *
     * @throws IllegalArgumentException if id's list is null
     */
    @Override
    public void lock(List<T> ids) {
        lock(ids, null);
    }

    /**
     * Releases locks on the id's list. Ignore null elements.
     *
     * @param ids id's list for lock
     *
     * @throws IllegalArgumentException if id's list is null
     * @throws IllegalMonitorStateException if the current thread does not
     *         hold this lock or any of ids never was locked
     */
    @Override
    public void unlock(List<T> ids) {
        unlock(ids, null);
    }

    /**
     * Execute the supplier and return the value. Locks on all id's from list are
     * acquired while the supplier is executing. Null elements are ignored.
     *
     * List is used as is. Use sorted list or {@link ReentrantEntityLocker#execute(List, Comparator, Supplier)}
     * to prevent deadlock.
     *
     * The method uses list's copy for locking.
     * The method uses blocking lock.
     *
     * @param ids id's list for locking
     * @param supplier
     * @param <R> supplier's return type
     * @return the result of supplier
     *
     * @throws IllegalArgumentException if id's list or supplier are null
     */
    @Override
    public <R> R execute(List<T> ids, Supplier<R> supplier) {
        return execute(ids, null, supplier);
    }

    /**
     * Execute the consumer. Locks on all id's from list are
     * acquired while the consumer is executing. Null elements are ignored.
     *
     * List is used as is. Use sorted list or {@link ReentrantEntityLocker#execute(List, Comparator, Consumer)}
     * to prevent deadlock.
     *
     * The method uses list's copy for locking.
     * The method uses blocking lock.
     *
     * @param ids id's list for locking
     * @param consumer
     *
     * @throws IllegalArgumentException if id's list or consumer are null
     */
    @Override
    public void execute(List<T> ids, Consumer<? super List<? extends T>> consumer) {
        execute(ids, null, consumer);
    }

    /**
     * Execute the function. Locks on all id's from list are
     * acquired while the supplier is executing. Null elements are ignored.
     *
     * List is used as is. Use sorted list or {@link ReentrantEntityLocker#execute(List, Comparator, Function)}
     * to prevent deadlock.
     *
     * The method uses list's copy for locking.
     * The method uses blocking lock.
     *
     * @param ids id's list for locking
     * @param function
     * @param <R> function's return type
     * @return the result of function
     *
     * @throws IllegalArgumentException if id's list or function are null
     */
    @Override
    public <R> R execute(List<T> ids, Function<? super List<? extends T>, R> function) {
        return execute(ids, null, function);
    }

    /**
     * Acquires the locks on the id's list, which will be sorted by comparator. Null elements are ignored.
     *
     * If comparator is null, the list won't be sorted.
     *
     * This method is blocking. use {@link ReentrantEntityLocker#tryLock(List, Comparator)} or
     * {@link ReentrantEntityLocker#tryLock(List, Comparator)}
     *
     * @param ids id's list for lock
     * @param comparator comparator for sorting
     *
     * @throws IllegalArgumentException if id is null
     */
    @Override
    public void lock(List<T> ids, Comparator<? super T> comparator) {
        checkForNull(ids, "Ids list mustn't be null");
        List<T> sortedList = copyAndSortList(ids, comparator);
        List<T> lockedList = new ArrayList<>();
        try {
            for (T t : sortedList) {
                lock(t);
                lockedList.add(t);
            }
        }
        catch (Exception e) {
            lockedList.forEach(this::unlock);
            throw e;
        }
    }

    /**
     * Release locks on all ids from list, which will be sorted by comparator. Null elements are ignored.
     *
     * If comparator is null, the list won't be sorted.
     *
     * @param ids id's list for unlocking
     * @param comparator comparator for sorting
     *
     * @throws IllegalArgumentException if id is null
     * @throws IllegalMonitorStateException if the current thread does not
     *         hold this lock or any of ids never was locked
     */
    @Override
    public void unlock(List<T> ids, Comparator<? super T> comparator) {
        checkForNull(ids, "Ids list mustn't be null");
        List<T> sortedList = copyAndSortList(ids, comparator != null ? comparator.reversed() : null);
        sortedList.forEach(this::unlock);
    }

    /**
     * Execute the supplier and return the value. Locks on all id's from list(sorted by comparator) are acquired while
     * the supplier is executing. Null elements are ignored.
     *
     * If comparator is null, the list won't be sorted.
     *
     * The method uses list's copy for locking.
     * The method uses blocking lock.
     *
     * @param ids id's list for locking
     * @param comparator comparator for sorting
     * @param supplier
     * @param <R> supplier's return type
     * @return the result of supplier
     *
     * @throws IllegalArgumentException if id's list or supplier are null
     */
    @Override
    public <R> R execute(List<T> ids, Comparator<? super T> comparator, Supplier<R> supplier) {
        checkForNull(supplier, "Supplier mustn't be null");
        checkForNull(ids, "Id's list mustn't be null");
        List<T> sortedList = copyAndSortList(ids, comparator);
        lock(sortedList);
        try {
           return supplier.get();
        }
        finally {
            Collections.reverse(sortedList);
            unlock(sortedList);
        }
    }

    /**
     * Execute the consumer. Locks on all id's from list(sorted by comparator) are acquired while the consumer is executing.
     * Null elements are ignored.
     *
     * If comparator is null, the list won't be sorted.
     *
     * The method uses list's copy for locking.
     * The method uses blocking lock.
     *
     * @param ids id's list for locking
     * @param consumer
     *
     * @throws IllegalArgumentException if id's list or consumer are null
     */
    @Override
    public void execute(List<T> ids, Comparator<? super T> comparator, Consumer<? super List<? extends T>> consumer) {
        checkForNull(consumer, "Consumer mustn't be null");
        checkForNull(ids, "Id's list mustn't be null");
        List<T> sortedList = copyAndSortList(ids, comparator);
        lock(sortedList);
        try {
            consumer.accept(ids);
        }
        finally {
            Collections.reverse(sortedList);
            unlock(sortedList);
        }
    }

    /**
     * Execute the function and return the value. Locks on all id's from list(sorted by comparator) are
     * acquired while the function is executing.Null elements are ignored.
     *
     * If comparator is null, the list won't be sorted.
     *
     * The method uses list's copy for locking.
     * The method uses blocking lock.
     *
     * @param ids id's list for locking
     * @param comparator
     * @param function
     * @param <R> function's return type
     * @return the result of function
     *
     * @throws IllegalArgumentException if id's list or function are null
     */
    @Override
    public <R> R execute(List<T> ids, Comparator<? super T> comparator, Function<? super List<? extends T>, R> function) {
        checkForNull(function, "Function mustn't be null");
        checkForNull(ids, "Id's list mustn't be null");
        List<T> sortedList = copyAndSortList(ids, comparator);
        lock(sortedList);
        try {
            return function.apply(ids);
        }
        finally {
            Collections.reverse(sortedList);
            unlock(sortedList);
        }
    }

    /**
     * Try to acquires lock one the id within the specified time.
     *
     * @param id id for lock
     * @param time waiting time
     * @param unit waiting time unit
     *
     * @return true if the id is locked, false - is not locked.
     * @throws InterruptedException if the current thread is interrupted
     *         while acquiring the lock (and interruption of lock
     *         acquisition is supported)
     * @throws IllegalArgumentException if id is null
     */
    @Override
    public boolean tryLock(T id, long time, TimeUnit unit) throws InterruptedException {
        checkForNull(id, "Can't lock on null object");
        boolean locked = readWriteLocksLock.readLock().tryLock(time, unit);
        boolean entityLocked = false;
        if (locked) {
            try {
                entityLocked = getLock(id).tryLock(time, unit);
            }
            finally {
                readWriteLocksLock.readLock().unlock();
            }
        }
        return entityLocked;
    }

    /**
     * Try to acquires lock one all ids from list within the specified time for each elements. List will be sorted
     * by comparator before the locking.
     * Null elements in list are ignored.
     *
     * Use only with sorted list to prevent to deadlock.
     *
     *
     * @param ids id's list for lock
     * @param time waiting time
     * @param unit waiting time unit
     *
     * @return true if all ids are locked, false - at least one id is not locked.
     * @throws InterruptedException if the current thread is interrupted
     *         while acquiring the lock (and interruption of lock
     *         acquisition is supported)
     * @throws IllegalArgumentException if id's list is null
     */
    @Override
    public boolean tryLock(List<T> ids, long time, TimeUnit unit) throws InterruptedException {
        return tryLock(ids, null, time, unit);
    }

    /**
     * Try to acquires lock one all ids from list within the specified time for each elements. List will be sorted
     * by comparator before the locking.
     * Null elements in list are ignored.
     * If comparator is null, the list won't be sorted.
     *
     *
     * @param ids id's list for lock
     * @param comparator comparator for sorting
     * @param time waiting time
     * @param unit waiting time unit
     *
     * @return true if all ids are locked, false - at least one id is not locked.
     * @throws InterruptedException if the current thread is interrupted
     *         while acquiring the lock (and interruption of lock
     *         acquisition is supported)
     * @throws IllegalArgumentException if id's list is null
     */
    @Override
    public boolean tryLock(List<T> ids, Comparator<? super T> comparator, long time, TimeUnit unit) throws InterruptedException {
        checkForNull(ids, "Ids list mustn't be null");
        List<T> sortedList = copyAndSortList(ids, comparator);
        List<T> lockedIds = new ArrayList<>(sortedList.size());
        try {
            for (T t : sortedList) {
                if (tryLock(t, time, unit)) {
                    lockedIds.add(t);
                } else {
                    break;
                }
            }
        }
        finally {
            if (sortedList.size() != lockedIds.size()) {
                unlock(lockedIds);
            }
        }
        return sortedList.size() == lockedIds.size();
    }

    /**
     * Try to acquires lock one the id.
     *
     * @param id id for lock
     *
     * @return true if the id is locked, false - is not locked.
     * @throws InterruptedException if the current thread is interrupted
     *         while acquiring the lock (and interruption of lock
     *         acquisition is supported)
     * @throws IllegalArgumentException if id is null
     */
    @Override
    public boolean tryLock(T id) throws InterruptedException {
        checkForNull(id, "Can't lock on null object");
        boolean locked = readWriteLocksLock.readLock().tryLock();
        boolean entityLocked = false;
        if (locked) {
            try {
                entityLocked = getLock(id).tryLock();
            }
            finally {
                readWriteLocksLock.readLock().unlock();
            }
        }
        return entityLocked;
    }

    /**
     * Try to acquires lock one all ids from list. List will be sorted
     * by comparator before the locking.
     * Null elements in list are ignored.
     *
     * Use only with sorted list to prevent to deadlock.
     *
     *
     * @param ids id's list for lock
     *
     * @return true if all ids are locked, false - at least one id is not locked.
     * @throws InterruptedException if the current thread is interrupted
     *         while acquiring the lock (and interruption of lock
     *         acquisition is supported)
     * @throws IllegalArgumentException if id's list is null
     */
    @Override
    public boolean tryLock(List<T> ids) throws InterruptedException {
        return tryLock(ids, null);
    }

    /**
     * Try to acquires lock one all ids from list. List will be sorted
     * by comparator before the locking.
     * Null elements in list are ignored.
     * If comparator is null, the list won't be sorted.
     *
     *
     * @param ids id's list for lock
     * @param comparator comparator for sorting
     *
     * @return true if all ids are locked, false - at least one id is not locked.
     * @throws InterruptedException if the current thread is interrupted
     *         while acquiring the lock (and interruption of lock
     *         acquisition is supported)
     * @throws IllegalArgumentException if id's list is null
     */
    @Override
    public boolean tryLock(List<T> ids, Comparator<? super T> comparator) throws InterruptedException {
        checkForNull(ids, "Ids list mustn't be null");
        List<T> sortedList = copyAndSortList(ids, comparator);
        List<T> lockedIds = new ArrayList<>(sortedList.size());
        try {
            for (T t : sortedList) {
                if (tryLock(t)) {
                    lockedIds.add(t);
                } else {
                    break;
                }
            }
        }
        finally {
            if (sortedList.size() != lockedIds.size()) {
                unlock(lockedIds);
            }
        }
        return sortedList.size() == lockedIds.size();
    }

    private List<T> copyAndSortList(List<T> ids, Comparator<? super T> comparator) {
        List<T> copy = getListWithoutNullAndDistinct(ids);
        if (comparator != null) {
            copy.sort(comparator);
        }
        return copy;
    }

    private List<T> getListWithoutNullAndDistinct(List<T> list) {
        return list.stream()
                .filter(Objects::nonNull)
                .distinct()
                .collect(Collectors.toList());
    }

    private void checkForNull(Object nullable, String message) {
        if (nullable == null) {
            throw new IllegalArgumentException(message);
        }
    }

    private Lock getLock(T id) {
        return locks.computeIfAbsent(id, this::createLock);
    }

    /**
     * Creating lock class.
     *
     * When override the method, {@link ReentrantEntityLocker#shouldBeRemoved(Lock)} must be overriden too.
     *
     * @param id
     * @return
     */
    protected Lock createLock(T id) {
        return new ReentrantLock(fair);
    }

    /**
     * Method for cleaning used locks. The method use {@link ReentrantEntityLocker#shouldBeRemoved(Lock)}.
     * If {@link ReentrantEntityLocker#createLock(Object)} is overriden, {@link ReentrantEntityLocker#shouldBeRemoved(Lock)}
     * have to be overriden too.
     */
    public void clearNotLockedLocks() {
        readWriteLocksLock.writeLock().lock();
        try {
            locks.entrySet().removeIf(entry -> shouldBeRemoved(entry.getValue()));
        }
        finally {
            readWriteLocksLock.writeLock().unlock();
        }
    }

    /**
     * Check that lock can be deleted from map.
     *
     * When override the method, {@link ReentrantEntityLocker#createLock(Object)} must be overriden too.
     *
     * @param lock
     * @return
     */
    protected boolean shouldBeRemoved(Lock lock) {
        if (lock == null) {
            return true;
        }
        ReentrantLock reentrantLock = (ReentrantLock) lock;
        return !reentrantLock.isLocked() && !reentrantLock.hasQueuedThreads();
    }
}
