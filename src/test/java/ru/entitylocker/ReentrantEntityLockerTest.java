package ru.entitylocker;

import org.junit.Assert;
import org.junit.Test;
import ru.entitylocker.EntityLocker;
import ru.entitylocker.ReentrantEntityLocker;

import java.lang.reflect.Field;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

import static org.mockito.Mockito.*;

public class ReentrantEntityLockerTest {

    @Test
    public void testLockUnlockOnSingleId() {
        Lock lock = spy(new ReentrantLock());
        EntityLocker<Integer> locker = new ReentrantEntityLocker<Integer>() {
            @Override
            protected Lock createLock(Integer id) {
                return lock;
            }
        };
        Integer id = 1;
        locker.lock(id);
        locker.unlock(id);
        verify(lock, times(1)).lock();
        verify(lock, times(1)).unlock();
    }    
    
    @Test
    public void testLockNullId() {
        EntityLocker<Integer> locker = new ReentrantEntityLocker<>();
        Integer id = null;
        try {
            locker.lock(id);
            Assert.fail("Lock on null is not allow");
        }
        catch (IllegalArgumentException e) {
        }
    }
    
    @Test
    public void testUnlockNullId() {
        EntityLocker<Integer> locker = new ReentrantEntityLocker<>();
        Integer id = null;
        try {
            locker.unlock(id);
            Assert.fail("Unlock on null is not allow");
        }
        catch (IllegalArgumentException e) {
        }
    }
    
    @Test
    public void testUnlockNotLockedId() {
        EntityLocker<Integer> locker = new ReentrantEntityLocker<>();
        try {
            locker.unlock(1);
            Assert.fail("Unlock on not Locked id is not allow");
        }
        catch (IllegalMonitorStateException e) {
        }
    }

    @Test
    public void testLockUnlockOnTheNotSortingList() {
        List<Integer> lockedList = new ArrayList<>();
        List<Integer> unlockedList = new ArrayList<>();
        EntityLocker<Integer> locker = getMockedIntegerEntityLocker(lockedList, unlockedList);
        List<Integer> inputList = Arrays.asList(3,1,5,2, null);
        locker.lock(inputList);
        locker.unlock(inputList);
        Assert.assertEquals("Not all elements was locked", Arrays.asList(3,1,5,2), lockedList);
        Assert.assertEquals("Not all elements was unlocked", Arrays.asList(3,1,5,2), unlockedList);
    }

    @Test
    public void testLockNullList() {
        EntityLocker<Integer> locker = new ReentrantEntityLocker<>();
        List<Integer> list = null;
        try {
            locker.lock(list);
            Assert.fail("Lock on null list is not allow");
        }
        catch (IllegalArgumentException e) {
        }
    }

    @Test
    public void testUnlockNullList() {
        EntityLocker<Integer> locker = new ReentrantEntityLocker<>();
        List<Integer> list = null;
        try {
            locker.unlock(list);
            Assert.fail("Unlock on null list is not allow");
        }
        catch (IllegalArgumentException e) {
        }
    }

    @Test
    public void testUnlockNotLockedIdInList() {
        EntityLocker<Integer> locker = new ReentrantEntityLocker<>();
        List<Integer> list = Collections.singletonList(2);
        try {
            locker.unlock(list);
            Assert.fail("Unlock on not locked id is not allow");
        }
        catch (IllegalMonitorStateException e) {
        }
    }

    @Test
    public void testLockUnlockOnTheSortingList() {
        List<Integer> lockedList = new ArrayList<>();
        List<Integer> unlockedList = new ArrayList<>();
        EntityLocker<Integer> locker = getMockedIntegerEntityLocker(lockedList, unlockedList);
        List<Integer> inputList = Arrays.asList(3,1,5,2, null);
        locker.lock(inputList, Comparator.naturalOrder());
        locker.unlock(inputList, Comparator.naturalOrder());
        Assert.assertEquals("Not all elements was locked", Arrays.asList(1,2,3,5), lockedList);
        Assert.assertEquals("Not all elements was unlocked", Arrays.asList(5,3,2,1), unlockedList);
        Assert.assertEquals("Input list was changed", Arrays.asList(3,1,5,2, null), inputList);
    }

    private EntityLocker<Integer> getMockedIntegerEntityLocker(List<Integer> lockedList, List<Integer> unlockedList) {
        return new ReentrantEntityLocker<Integer>() {
                protected Lock createLock(Integer id) {
                    return new ReentrantLock() {
                        @Override
                        public void lock() {
                            lockedList.add(id);
                        }
                        @Override
                        public void unlock() {
                            unlockedList.add(id);
                        }
                    };
                }
            };
    }

    @Test
    public void testLockUnlockOnTheSortingListAndComparatorIsNull() {
        List<Integer> lockedList = new ArrayList<>();
        List<Integer> unlockedList = new ArrayList<>();
        EntityLocker<Integer> locker = getMockedIntegerEntityLocker(lockedList, unlockedList);
        List<Integer> inputList = Arrays.asList(3,1,5,2, null);
        locker.lock(inputList, null);
        Assert.assertEquals("Input list was changed", Arrays.asList(3,1,5,2, null), inputList);
        locker.unlock(inputList, null);
        Assert.assertEquals("Not all elements was locked", Arrays.asList(3,1,5,2), lockedList);
        Assert.assertEquals("Not all elements was unlocked", Arrays.asList(3,1,5,2), unlockedList);
        Assert.assertEquals("Input list was changed", Arrays.asList(3,1,5,2, null), inputList);
    }

    @Test
    public void testLockNullListWithComparator() {
        EntityLocker<Integer> locker = new ReentrantEntityLocker<>();
        List<Integer> list = null;
        try {
            locker.lock(list, Comparator.naturalOrder());
            Assert.fail("Lock on null list is not allow");
        }
        catch (IllegalArgumentException e) {
        }
    }

    @Test
    public void testUnlockNullListWithComparator() {
        EntityLocker<Integer> locker = new ReentrantEntityLocker<>();
        List<Integer> list = null;
        try {
            locker.unlock(list, Comparator.naturalOrder());
            Assert.fail("Unlock on null list is not allow");
        }
        catch (IllegalArgumentException e) {
        }
    }

    @Test
    public void testUnlockNotLockedIdInListWithComparator() {
        EntityLocker<Integer> locker = new ReentrantEntityLocker<>();
        List<Integer> list = Collections.singletonList(2);
        try {
            locker.unlock(list, Comparator.naturalOrder());
            Assert.fail("Unlock on not locked id is not allow");
        }
        catch (IllegalMonitorStateException e) {
        }
    }

    @Test
    public void testLockUnlockIdTwice() {
        List<Integer> lockedList = new ArrayList<>();
        List<Integer> unlockedList = new ArrayList<>();
        EntityLocker<Integer> locker = getMockedIntegerEntityLocker(lockedList, unlockedList);
        locker.lock(1);
        locker.lock(1);
        locker.unlock(1);
        locker.unlock(1);
        Assert.assertEquals("Not all elements was locked", Arrays.asList(1,1), lockedList);
        Assert.assertEquals("Not all elements was unlocked", Arrays.asList(1,1), unlockedList);
    }

    @Test
    public void testLockUnlockListWithSameId() {
        List<Integer> lockedList = new ArrayList<>();
        List<Integer> unlockedList = new ArrayList<>();
        EntityLocker<Integer> locker = getMockedIntegerEntityLocker(lockedList, unlockedList);
        List<Integer> inputList = Arrays.asList(1,1,1,1);
        locker.lock(inputList);
        locker.unlock(inputList);
        Assert.assertEquals("Not all elements was locked", Arrays.asList(1), lockedList);
        Assert.assertEquals("Not all elements was unlocked", Arrays.asList(1), unlockedList);
        Assert.assertEquals("Input list was changed", Arrays.asList(1,1,1,1), inputList);
    }

    @Test
    public void testLockUnlockListWithSameIdWithComparator() {
        List<Integer> lockedList = new ArrayList<>();
        List<Integer> unlockedList = new ArrayList<>();
        EntityLocker<Integer> locker = getMockedIntegerEntityLocker(lockedList, unlockedList);
        List<Integer> inputList = Arrays.asList(1,1,1,1);
        locker.lock(inputList, Comparator.naturalOrder());
        locker.unlock(inputList);
        Assert.assertEquals("Not all elements was locked", Arrays.asList(1), lockedList);
        Assert.assertEquals("Not all elements was unlocked", Arrays.asList(1), unlockedList);
        Assert.assertEquals("Input list was changed", Arrays.asList(1,1,1,1), inputList);
    }

    @Test
    public void testExecuteConsumerWithIdLock() {
        List<Integer> lockedList = new ArrayList<>();
        List<Integer> unlockedList = new ArrayList<>();
        EntityLocker<Integer> locker = getMockedIntegerEntityLocker(lockedList, unlockedList);
        List<Integer> executedList = new ArrayList<>();
        locker.execute(1, id -> {executedList.add(id);});
        Assert.assertEquals("Not all elements was locked", Arrays.asList(1), lockedList);
        Assert.assertEquals("Not all elements was unlocked", Arrays.asList(1), unlockedList);
        Assert.assertEquals("Consumer won't be executed", Arrays.asList(1), executedList);
    }

    @Test
    public void testExecuteFunctionWithIdLock() {
        List<Integer> lockedList = new ArrayList<>();
        List<Integer> unlockedList = new ArrayList<>();
        EntityLocker<Integer> locker = getMockedIntegerEntityLocker(lockedList, unlockedList);
        String result = locker.execute(1, id -> {
            return String.valueOf(id);
        });
        Assert.assertEquals("Not all elements was locked", Arrays.asList(1), lockedList);
        Assert.assertEquals("Not all elements was unlocked", Arrays.asList(1), unlockedList);
        Assert.assertEquals("Function won't be executed", "1", result);
    }

    @Test
    public void testExecuteSupplierWithIdLock() {
        List<Integer> lockedList = new ArrayList<>();
        List<Integer> unlockedList = new ArrayList<>();
        EntityLocker<Integer> locker = getMockedIntegerEntityLocker(lockedList, unlockedList);
        String result = locker.execute(1, () -> "result");
        Assert.assertEquals("Not all elements was locked", Arrays.asList(1), lockedList);
        Assert.assertEquals("Not all elements was unlocked", Arrays.asList(1), unlockedList);
        Assert.assertEquals("Function won't be executed", result, "result");
    }

    @Test
    public void testExecuteNullFunctionAndLockId() {
        List<Integer> lockedList = new ArrayList<>();
        List<Integer> unlockedList = new ArrayList<>();
        EntityLocker<Integer> locker = getMockedIntegerEntityLocker(lockedList, unlockedList);
        Function<Integer, String> f = null;
        try {
            locker.execute(1, f);
            Assert.fail("Null function mustn't be evaluated");
        }
        catch (IllegalArgumentException e) {

        }
        Assert.assertEquals("Not all elements was locked", Collections.emptyList(), lockedList);
        Assert.assertEquals("Not all elements was unlocked", Collections.emptyList(), unlockedList);
    }

    @Test
    public void testExecuteNullConsumerAndLockId() {
        List<Integer> lockedList = new ArrayList<>();
        List<Integer> unlockedList = new ArrayList<>();
        EntityLocker<Integer> locker = getMockedIntegerEntityLocker(lockedList, unlockedList);
        Consumer<Integer> c = null;
        try {
            locker.execute(1, c);
            Assert.fail("Null consumer mustn't be evaluated");
        }
        catch (IllegalArgumentException e) {

        }
        Assert.assertEquals("Not all elements was locked", Collections.emptyList(), lockedList);
        Assert.assertEquals("Not all elements was unlocked", Collections.emptyList(), unlockedList);
    }

    @Test
    public void testExecuteNullSupplierAndLockId() {
        List<Integer> lockedList = new ArrayList<>();
        List<Integer> unlockedList = new ArrayList<>();
        EntityLocker<Integer> locker = getMockedIntegerEntityLocker(lockedList, unlockedList);
        Supplier<Integer> s = null;
        try {
            locker.execute(1, s);
            Assert.fail("Null consumer mustn't be evaluated");
        }
        catch (IllegalArgumentException e) {

        }
        Assert.assertEquals("Not all elements was locked", Collections.emptyList(), lockedList);
        Assert.assertEquals("Not all elements was unlocked", Collections.emptyList(), unlockedList);
    }

    @Test
    public void testExecuteFunctionAndLockNullId() {
        List<Integer> lockedList = new ArrayList<>();
        List<Integer> unlockedList = new ArrayList<>();
        EntityLocker<Integer> locker = getMockedIntegerEntityLocker(lockedList, unlockedList);
        Function<Integer, String> f = id -> String.valueOf(id);
        try {
            locker.execute(null, f);
            Assert.fail("Null id mustn't be locked");
        }
        catch (IllegalArgumentException e) {

        }
        Assert.assertEquals("Not all elements was locked", Collections.emptyList(), lockedList);
        Assert.assertEquals("Not all elements was unlocked", Collections.emptyList(), unlockedList);
    }

    @Test
    public void testExecuteConsumerAndLockNullId() {
        List<Integer> lockedList = new ArrayList<>();
        List<Integer> unlockedList = new ArrayList<>();
        EntityLocker<Integer> locker = getMockedIntegerEntityLocker(lockedList, unlockedList);
        Consumer<Integer> c = id -> {};
        try {
            locker.execute(null, c);
            Assert.fail("Null id mustn't be locked");
        }
        catch (IllegalArgumentException e) {

        }
        Assert.assertEquals("Not all elements was locked", Collections.emptyList(), lockedList);
        Assert.assertEquals("Not all elements was unlocked", Collections.emptyList(), unlockedList);
    }

    @Test
    public void testExecuteSupplierAndLockNullId() {
        List<Integer> lockedList = new ArrayList<>();
        List<Integer> unlockedList = new ArrayList<>();
        EntityLocker<Integer> locker = getMockedIntegerEntityLocker(lockedList, unlockedList);
        Supplier<Integer> s = () -> 1;
        Integer id = null;
        try {
            locker.execute(id, s);
            Assert.fail("Null id mustn't be evaluated");
        }
        catch (IllegalArgumentException e) {

        }
        Assert.assertEquals("Not all elements was locked", Collections.emptyList(), lockedList);
        Assert.assertEquals("Not all elements was unlocked", Collections.emptyList(), unlockedList);
    }

    @Test
    public void testExecuteConsumerWithListIdLock() {
        List<Integer> lockedList = new ArrayList<>();
        List<Integer> unlockedList = new ArrayList<>();
        EntityLocker<Integer> locker = getMockedIntegerEntityLocker(lockedList, unlockedList);
        List<Integer> inputList = Arrays.asList(2,5, null,3,7);
        List<String> stringList = new ArrayList<>();
        locker.execute(inputList, list -> {stringList.add("executed");});
        Assert.assertEquals("Not all elements was locked", Arrays.asList(2,5,3,7), lockedList);
        Assert.assertEquals("Not all elements was unlocked", Arrays.asList(7,3,5,2), unlockedList);
        Assert.assertEquals("Consumer won't be executed", Arrays.asList("executed"), stringList);
        Assert.assertEquals("Input list was changed", Arrays.asList(2,5, null,3,7), inputList);
    }

    @Test
    public void testExecuteFunctionWithListIdLock() {
        List<Integer> lockedList = new ArrayList<>();
        List<Integer> unlockedList = new ArrayList<>();
        EntityLocker<Integer> locker = getMockedIntegerEntityLocker(lockedList, unlockedList);
        List<Integer> inputList = Arrays.asList(2,5, null,3,7);
        Function<List<? extends Integer>, List<Integer>> f = ids -> (List<Integer>)ids;
        List<Integer> result = locker.execute(inputList, f);
        Assert.assertEquals("Not all elements was locked", Arrays.asList(2,5,3,7), lockedList);
        Assert.assertEquals("Not all elements was unlocked", Arrays.asList(7,3,5,2), unlockedList);
        Assert.assertEquals("Function won't be executed", inputList, result);
        Assert.assertEquals("Input list was changed", Arrays.asList(2,5, null,3,7), inputList);
    }

    @Test
    public void testExecuteSupplierWithListIdLock() {
        List<Integer> lockedList = new ArrayList<>();
        List<Integer> unlockedList = new ArrayList<>();
        EntityLocker<Integer> locker = getMockedIntegerEntityLocker(lockedList, unlockedList);
        List<Integer> inputList = Arrays.asList(2,5, null,3,7);
        String result = locker.execute(inputList, () -> "result");
        Assert.assertEquals("Not all elements was locked", Arrays.asList(2,5,3,7), lockedList);
        Assert.assertEquals("Not all elements was unlocked", Arrays.asList(7,3,5,2), unlockedList);
        Assert.assertEquals("Function won't be executed", result, "result");
        Assert.assertEquals("Input list was changed", Arrays.asList(2,5, null,3,7), inputList);
    }

    @Test
    public void testExecuteNullFunctionAndLockIdList() {
        List<Integer> lockedList = new ArrayList<>();
        List<Integer> unlockedList = new ArrayList<>();
        EntityLocker<Integer> locker = getMockedIntegerEntityLocker(lockedList, unlockedList);
        List<Integer> inputList = Arrays.asList(2,5, null,3,7);
        Function<List<? extends Integer>, String> f = null;
        try {
            locker.execute(inputList, f);
            Assert.fail("Null function mustn't be evaluated");
        }
        catch (IllegalArgumentException e) {

        }
        Assert.assertEquals("Not all elements was locked", Collections.emptyList(), lockedList);
        Assert.assertEquals("Not all elements was unlocked", Collections.emptyList(), unlockedList);
    }

    @Test
    public void testExecuteNullConsumerAndLockIdList() {
        List<Integer> lockedList = new ArrayList<>();
        List<Integer> unlockedList = new ArrayList<>();
        EntityLocker<Integer> locker = getMockedIntegerEntityLocker(lockedList, unlockedList);
        List<Integer> inputList = Arrays.asList(2,5, null,3,7);
        Consumer<List<? extends Integer>> c = null;
        try {
            locker.execute(inputList, c);
            Assert.fail("Null consumer mustn't be evaluated");
        }
        catch (IllegalArgumentException e) {

        }
        Assert.assertEquals("Not all elements was locked", Collections.emptyList(), lockedList);
        Assert.assertEquals("Not all elements was unlocked", Collections.emptyList(), unlockedList);
    }

    @Test
    public void testExecuteNullSupplierAndLockIdList() {
        List<Integer> lockedList = new ArrayList<>();
        List<Integer> unlockedList = new ArrayList<>();
        EntityLocker<Integer> locker = getMockedIntegerEntityLocker(lockedList, unlockedList);
        List<Integer> inputList = Arrays.asList(2,5, null,3,7);
        Supplier<String> s = null;
        try {
            locker.execute(inputList, s);
            Assert.fail("Null consumer mustn't be evaluated");
        }
        catch (IllegalArgumentException e) {

        }
        Assert.assertEquals("Not all elements was locked", Collections.emptyList(), lockedList);
        Assert.assertEquals("Not all elements was unlocked", Collections.emptyList(), unlockedList);
    }

    @Test
    public void testExecuteFunctionAndLockNullIdList() {
        List<Integer> lockedList = new ArrayList<>();
        List<Integer> unlockedList = new ArrayList<>();
        EntityLocker<Integer> locker = getMockedIntegerEntityLocker(lockedList, unlockedList);
        List<Integer> inputList = null;
        Function<List<? extends Integer>, String> f = String::valueOf;
        try {
            locker.execute(inputList, f);
            Assert.fail("Null id's list mustn't be locked");
        }
        catch (IllegalArgumentException e) {

        }
        Assert.assertEquals("Not all elements was locked", Collections.emptyList(), lockedList);
        Assert.assertEquals("Not all elements was unlocked", Collections.emptyList(), unlockedList);
    }

    @Test
    public void testExecuteConsumerAndLockNullIdList() {
        List<Integer> lockedList = new ArrayList<>();
        List<Integer> unlockedList = new ArrayList<>();
        EntityLocker<Integer> locker = getMockedIntegerEntityLocker(lockedList, unlockedList);
        Consumer<List<? extends Integer>> c = id -> {};
        List<Integer> inputList = null;
        try {
            locker.execute(inputList, c);
            Assert.fail("Null id's list mustn't be locked");
        }
        catch (IllegalArgumentException e) {

        }
        Assert.assertEquals("Not all elements was locked", Collections.emptyList(), lockedList);
        Assert.assertEquals("Not all elements was unlocked", Collections.emptyList(), unlockedList);
    }

    @Test
    public void testExecuteSupplierAndLockNullIdList() {
        List<Integer> lockedList = new ArrayList<>();
        List<Integer> unlockedList = new ArrayList<>();
        EntityLocker<Integer> locker = getMockedIntegerEntityLocker(lockedList, unlockedList);
        Supplier<Integer> s = () -> 1;
        List<Integer> ids = null;
        try {
            locker.execute(ids, s);
            Assert.fail("Null id's list mustn't be locked");
        }
        catch (IllegalArgumentException e) {

        }
        Assert.assertEquals("Not all elements was locked", Collections.emptyList(), lockedList);
        Assert.assertEquals("Not all elements was unlocked", Collections.emptyList(), unlockedList);
    }

    @Test
    public void testTryLockingIdOneThread() throws InterruptedException {
        ReentrantEntityLocker<Integer> spyEntityLocker = spy(new ReentrantEntityLocker<>());
        ReentrantLock spyReentrantLock = spy(new ReentrantLock());
        when(spyEntityLocker.createLock(anyInt())).thenReturn(spyReentrantLock);
        spyEntityLocker.tryLock(1, 10, TimeUnit.SECONDS);
        spyEntityLocker.unlock(1);
        verify(spyEntityLocker, times(1)).tryLock(1, 10, TimeUnit.SECONDS);
    }

    @Test
    public void testTryLockingNullIdOneThread() throws InterruptedException {
        ReentrantEntityLocker<Integer> spyEntityLocker = new ReentrantEntityLocker<>();
        Integer id = null;
        try{
            spyEntityLocker.tryLock(id, 10, TimeUnit.SECONDS);
            Assert.fail("mustn't lock on null object");
        }
        catch (IllegalArgumentException e) {
        }
    }

    @Test
    public void testTryLockingListIdOneThread() throws InterruptedException {
        ReentrantEntityLocker<Integer> spyEntityLocker = spy(new ReentrantEntityLocker<>());
        ReentrantLock spyReentrantLock = spy(new ReentrantLock());
        when(spyEntityLocker.createLock(anyInt())).thenReturn(spyReentrantLock);
        spyEntityLocker.tryLock(Arrays.asList(1,2,3,4,4,null), 10, TimeUnit.SECONDS);
        spyEntityLocker.unlock(Arrays.asList(1,2,3,4));
        verify(spyEntityLocker, times(1)).tryLock(1, 10, TimeUnit.SECONDS);
        verify(spyEntityLocker, times(1)).tryLock(2, 10, TimeUnit.SECONDS);
        verify(spyEntityLocker, times(1)).tryLock(3, 10, TimeUnit.SECONDS);
        verify(spyEntityLocker, times(1)).tryLock(4, 10, TimeUnit.SECONDS);
    }

    @Test
    public void testTryLockingNullListIdOneThread() throws InterruptedException {
        ReentrantEntityLocker<Integer> entityLocker = new ReentrantEntityLocker<>();
        List<Integer> ids = null;
        try{
            entityLocker.tryLock(ids, 10, TimeUnit.SECONDS);
            Assert.fail("mustn't lock on null object");
        }
        catch (IllegalArgumentException e) {
        }
    }

    @Test
    public void testTryLockingListIdAndComparatorOneThread() throws InterruptedException {
        ReentrantEntityLocker<Integer> spyEntityLocker = spy(new ReentrantEntityLocker<>());
        ReentrantLock spyReentrantLock = spy(new ReentrantLock());
        when(spyEntityLocker.createLock(anyInt())).thenReturn(spyReentrantLock);
        spyEntityLocker.tryLock(Arrays.asList(1,2,3,4,4,null), Comparator.naturalOrder(), 10, TimeUnit.SECONDS);
        spyEntityLocker.unlock(Arrays.asList(1,2,3,4));
        verify(spyEntityLocker, times(1)).tryLock(1, 10, TimeUnit.SECONDS);
        verify(spyEntityLocker, times(1)).tryLock(2, 10, TimeUnit.SECONDS);
        verify(spyEntityLocker, times(1)).tryLock(3, 10, TimeUnit.SECONDS);
        verify(spyEntityLocker, times(1)).tryLock(4, 10, TimeUnit.SECONDS);
    }

    @Test
    public void testTryLockingNullListIdWithComparatorOneThread() throws InterruptedException {
        ReentrantEntityLocker<Integer> entityLocker = new ReentrantEntityLocker<>();
        List<Integer> ids = null;
        try{
            entityLocker.tryLock(ids, Comparator.naturalOrder(), 10, TimeUnit.SECONDS);
            Assert.fail("mustn't lock on null object");
        }
        catch (IllegalArgumentException e) {
        }
    }

    @Test
    public void testTryLockingIdOneThreadWithoutTime() throws InterruptedException {
        ReentrantEntityLocker<Integer> spyEntityLocker = spy(new ReentrantEntityLocker<>());
        ReentrantLock spyReentrantLock = spy(new ReentrantLock());
        when(spyEntityLocker.createLock(anyInt())).thenReturn(spyReentrantLock);
        spyEntityLocker.tryLock(1);
        spyEntityLocker.unlock(1);
        verify(spyEntityLocker, times(1)).tryLock(1);
    }

    @Test
    public void testTryLockingNullIdOneThreadWithoutTime() throws InterruptedException {
        ReentrantEntityLocker<Integer> spyEntityLocker = new ReentrantEntityLocker<>();
        Integer id = null;
        try{
            spyEntityLocker.tryLock(id);
            Assert.fail("mustn't lock on null object");
        }
        catch (IllegalArgumentException e) {
        }
    }

    @Test
    public void testTryLockingListIdOneThreadWithoutTime() throws InterruptedException {
        ReentrantEntityLocker<Integer> spyEntityLocker = spy(new ReentrantEntityLocker<>());
        ReentrantLock spyReentrantLock = spy(new ReentrantLock());
        when(spyEntityLocker.createLock(anyInt())).thenReturn(spyReentrantLock);
        spyEntityLocker.tryLock(Arrays.asList(1,2,3,4,4,null));
        spyEntityLocker.unlock(Arrays.asList(1,2,3,4));
        verify(spyEntityLocker, times(1)).tryLock(1);
        verify(spyEntityLocker, times(1)).tryLock(2);
        verify(spyEntityLocker, times(1)).tryLock(3);
        verify(spyEntityLocker, times(1)).tryLock(4);
    }

    @Test
    public void testTryLockingNullListIdOneThreadWithoutTime() throws InterruptedException {
        ReentrantEntityLocker<Integer> entityLocker = new ReentrantEntityLocker<>();
        List<Integer> ids = null;
        try{
            entityLocker.tryLock(ids);
            Assert.fail("mustn't lock on null object");
        }
        catch (IllegalArgumentException e) {
        }
    }

    @Test
    public void testTryLockingListIdAndComparatorOneThreadWithoutTime() throws InterruptedException {
        ReentrantEntityLocker<Integer> spyEntityLocker = spy(new ReentrantEntityLocker<>());
        ReentrantLock spyReentrantLock = spy(new ReentrantLock());
        when(spyEntityLocker.createLock(anyInt())).thenReturn(spyReentrantLock);
        spyEntityLocker.tryLock(Arrays.asList(1,2,3,4,4,null), Comparator.naturalOrder());
        spyEntityLocker.unlock(Arrays.asList(1,2,3,4));
        verify(spyEntityLocker, times(1)).tryLock(1);
        verify(spyEntityLocker, times(1)).tryLock(2);
        verify(spyEntityLocker, times(1)).tryLock(3);
        verify(spyEntityLocker, times(1)).tryLock(4);
    }

    @Test
    public void testTryLockingNullListIdWithComparatorOneThreadWithoutTime() throws InterruptedException {
        ReentrantEntityLocker<Integer> entityLocker = new ReentrantEntityLocker<>();
        List<Integer> ids = null;
        try{
            entityLocker.tryLock(ids, Comparator.naturalOrder());
            Assert.fail("mustn't lock on null object");
        }
        catch (IllegalArgumentException e) {
        }
    }

    @Test
    public void testTryLockListIfOneOfObjectIsLockedByAnotherObject() throws InterruptedException, NoSuchFieldException, IllegalAccessException {
        ReentrantEntityLocker<Integer> entityLocker = new ReentrantEntityLocker<>();
        entityLocker.lock(4);
        boolean[] result = {true};
        Thread thread = new Thread(() -> {
            try {
                result[0] = entityLocker.tryLock(Arrays.asList(1, 2, 3, 4), 1, TimeUnit.NANOSECONDS);
            } catch (InterruptedException e) {
            }
        });
        thread.start();
        thread.join();
        Assert.assertFalse("Ids shouldn't be locked", result[0]);
        Map<Integer, ReentrantLock> locks = getIntegerReentrantLockMap(entityLocker);
        boolean isLocked = locks.get(1).isLocked() ||
                locks.get(2).isLocked() ||
                locks.get(3).isLocked();
        Assert.assertFalse("Ids should be unlocked", isLocked);isLocked = locks.get(4).isLocked() || locks.get(4).isHeldByCurrentThread();
        Assert.assertTrue("Id 4 should be locked by main thread", isLocked);
    }
    @Test
    public void testTryLockIdIfItIsLockedByAnotherObject() throws InterruptedException, NoSuchFieldException, IllegalAccessException {
        ReentrantEntityLocker<Integer> entityLocker = new ReentrantEntityLocker<>();
        entityLocker.lock(4);
        boolean[] result = {true};
        Thread thread = new Thread(() -> {
            try {
                result[0] = entityLocker.tryLock(Arrays.asList(4), 1, TimeUnit.NANOSECONDS);
            } catch (InterruptedException e) {
            }
        });
        thread.start();
        thread.join();
        Assert.assertFalse("Ids shouldn't be locked", result[0]);
        Map<Integer, ReentrantLock> locks = getIntegerReentrantLockMap(entityLocker);
        boolean isLocked = locks.get(4).isLocked() || locks.get(4).isHeldByCurrentThread();
        Assert.assertTrue("Id should be locked by main thread", isLocked);
    }

    private Map<Integer, ReentrantLock> getIntegerReentrantLockMap(ReentrantEntityLocker<Integer> entityLocker) throws NoSuchFieldException, IllegalAccessException {
        Field locksField = ReentrantEntityLocker.class.getDeclaredField("locks");
        locksField.setAccessible(true);
        return (Map<Integer, ReentrantLock>)locksField.get(entityLocker);
    }

    @Test
    public void testClearLocksLockedItems() throws InterruptedException, NoSuchFieldException, IllegalAccessException {
        ReentrantEntityLocker<Integer> entityLocker = new ReentrantEntityLocker<>();
        entityLocker.lock(4);
        entityLocker.clearNotLockedLocks();
        Map<Integer, ReentrantLock> integerReentrantLockMap = getIntegerReentrantLockMap(entityLocker);
        Assert.assertEquals("Map shouldn't be empty", 1, integerReentrantLockMap.size());
    }

    @Test
    public void testClearLocksUnlockedItems() throws InterruptedException, NoSuchFieldException, IllegalAccessException {
        ReentrantEntityLocker<Integer> entityLocker = new ReentrantEntityLocker<>();
        entityLocker.lock(4);
        entityLocker.unlock(4);
        entityLocker.clearNotLockedLocks();
        Map<Integer, ReentrantLock> integerReentrantLockMap = getIntegerReentrantLockMap(entityLocker);
        Assert.assertEquals("Map should be empty", 0, integerReentrantLockMap.size());
    }

    @Test
    public void testClearLocksMixed() throws InterruptedException, NoSuchFieldException, IllegalAccessException {
        ReentrantEntityLocker<Integer> entityLocker = new ReentrantEntityLocker<>();
        entityLocker.lock(4);
        entityLocker.lock(1);
        entityLocker.unlock(4);
        entityLocker.clearNotLockedLocks();
        Map<Integer, ReentrantLock> integerReentrantLockMap = getIntegerReentrantLockMap(entityLocker);
        Assert.assertEquals("Map shouldn't be empty", 1, integerReentrantLockMap.size());
    }
}
